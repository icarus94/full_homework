<?php
    include 'functions.php';
    
    function createDropDownMenu($tipZadatka){
        if($tipZadatka=="student"){
            return getDataFromText("gradovi.txt");
        }elseif ($tipZadatka=="organizacija") {
             return getDataFromText("gradovi.txt");
        }elseif ($tipZadatka=="faktura") {
             return getDataFromText("faktura.txt");
        }else{
            echo "Losa obrada!";
        }
    }
    
    function generateFirstForm($tipZadatka , $parameter1 , $parameter2 , $parameter3 , $parameter4){
      $type=null;
      if($tipZadatka=="student"){
        $type="formaStudenta";
      }elseif ($tipZadatka=="faktura") {
        $type="formaFakture";
      }else{
        $type="formaOrganizacije";
      }

      $var=createDropDownMenu($tipZadatka);
      echo "<form name=".$type." action=".$_SERVER['PHP_SELF']." onsubmit=\"return validateForm()\"  method=\"post\">
                ".$parameter1.":<br>
                <input type=\"text\" name=\"formInput1\" value=".$_SESSION['value1FromForm1'].">
                <br>
                ".$parameter2.":<br>
                <input type=\"text\" name=\"formInput2\" value=".$_SESSION['value2FromForm1'].">
                <br>
                ".$parameter3.":<br>
                <input type=\"text\" name=\"formInput3\" value=".$_SESSION['value3FromForm1'].">
                <br>
                ".$parameter4.":<br>
                  ".$var."
                 <input type=\"submit\" value=\"Sacuvaj\" name=\"form1\">
              </form>";
    }
    
    function generateSecondForm($parameter1 , $parameter2){
      echo "<form name=\"unos_podataka_u_tabelu\" action=".$_SERVER['PHP_SELF']." method=\"post\">
                ".$parameter1.":<br>
                <input type=\"text\" name=\"form2Input1\"><br>
                ".$parameter2.":<br>
                <input type=\"text\" name=\"form2Input2\"><br>
                <input type=\"submit\" value=\"Unesi\" name=\"form2\">
            </form>";
    }
    
    function generateTable($parameter1 , $parameter2 , $parameter3, $type){
      $var=addingFromFormToTable($type); //ovde ubaciti dodatno putem funkcije
      echo "<table style=\"width:100%\">
              <tr>
                <td><b>".$parameter1."</b></td>
                <td><b>".$parameter2."<b></td> 
                <td><b>".$parameter3."</b></td>
              </tr>
              ".$var."
            </table>";
    }
    
     function addingFromFormToTable($type){
      $value=null;
      $i=1;
      global $arrayTable;
      $a=whichParameterByType($type);
      //var_dump($arrayTable);
      //die();
      while(isset($arrayTable[$_SESSION[$a]][$i]['1'])){
        $value= $value."<tr>
                  <td>".$i."</td>
                  <td>".$arrayTable[$_SESSION[$a]][$i]['1']."</td>
                  <td>".$arrayTable[$_SESSION[$a]][$i]['2']."</td>
                </tr>";
                $i++;
      }
      saveArray();
      return $value;
    }
    function whichParameterByType($type){
        if($type=="student"){
            return $a='value3FromForm1';
        }elseif ($type=="organizacija") {
             return $a='value1FromForm1';
        }elseif ($type=="faktura") {
             return $a='value3FromForm1';
        }
        return null;
    }
?>
