<?php
    session_start();
    if (isset($_SESSION['previous'])) {
        if (basename($_SERVER['PHP_SELF']) != $_SESSION['previous']) {
             session_destroy();
             session_start();
        }
    }
    $_SESSION['previous']=  basename($_SERVER['PHP_SELF']);
    include 'generator.php';
    getFakturaData();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="author" content="Trajko">
		<meta charset="UTF-8">
		<title>Administracija Fakture</title>
		<link rel="stylesheet" type="text/css" href="css/mycss.css">
	</head>
	<body>
		<center>
			<div>
				<h1>Forma za unos podataka o fakturi:</h1>
			</div>
		</center>
		<center>
			<div id="firstForm">
				<?php
					generateFirstForm("faktura","Broj računa","Datum","PIB","Organizacija");
				?>
			</div>
		</center>	
		<center>
			<div id="secondForm">
				<h1>Stavke fakture</h1>
				<?php
					generateTable("RB","NAZIV ARTIKLA","Količina","faktura");
				?>
			</div>
		</center>

		<center>
			<div id="thirdForm">
				<h1>Unos stavki fakture</h1>
				<?php
					generateSecondForm("Naziv artikla","Količina");
				?>
			</div>
		</center>
	</body>
</html>