<?php
    
    $arrayTable=null;
    if(file_exists('arrayForTable.bin')){
      $arrayTable=unserialize(file_get_contents('arrayForTable.bin'));
    }
    if(!(isset($_SESSION['value1FromForm1']) and isset($_SESSION['value2FromForm1']) and 
            isset($_SESSION['value3FromForm1']) and isset($_SESSION['value4FromForm1']))){
      $_SESSION['value1FromForm1']=null;
      $_SESSION['value2FromForm1']=null;
      $_SESSION['value3FromForm1']=null;
      $_SESSION['value4FromForm1']=null;
    }
    
    function getDataFromText($textlocation){
       $handle = fopen("$textlocation", "r");
       $data=null;
        if ($handle) {
           $lineNumber=1;
           $data="<select id=\"formInput4\" name=\"formInput4\" value=".$_SESSION['value4FromForm1'].">";
            while (($line = fgets($handle)) !== false) {
                if($line!=null && $line!="" && ctype_space($line)===false ){
                    if($lineNumber==$_SESSION['value4FromForm1']){
                      $data=$data."<option selected value=".$lineNumber.">$line</option>";
                      $lineNumber++;
                      continue;
                    }
                    $data=$data."<option value=".$lineNumber.">$line</option>";
                    $lineNumber++;
                }    
            }
            $data=$data."</select><br>";
        }
        fclose($handle);
        return $data;
    }
    
    function saveArray(){
        global $arrayTable;
        if($arrayTable!=null){
            file_put_contents('arrayForTable.bin', serialize($arrayTable));
        }
    }
    
    function getStudentData(){
        if($_POST==null){
          return;
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          
            if(isset($_POST['form1'])){
                $value1FromForm1 = $_POST['formInput1'];
                $value2FromForm1 = $_POST['formInput2'];
                $value3FromForm1 = $_POST['formInput3'];
                $value4FromForm1 = $_POST['formInput4'];
                if($value1FromForm1==null || $value2FromForm1==null || $value3FromForm1==null || $value4FromForm1==null){
                  return;
                }
                if(!ctype_alpha($value1FromForm1) || !ctype_alpha($value2FromForm1) || !ctype_digit($value3FromForm1) || !ctype_digit($value4FromForm1)){
                  return;
                }
                $_SESSION['value1FromForm1']=$value1FromForm1;
                $_SESSION['value2FromForm1']=$value2FromForm1;
                $_SESSION['value3FromForm1']=$value3FromForm1;
                $_SESSION['value4FromForm1']=$value4FromForm1;
                
            }
      
              if(isset($_POST['form2'])){
                 $value1FromForm2 =$_POST['form2Input1'];
                 $value2FromForm2 =$_POST['form2Input2'];
                 if($value1FromForm2==null || $value2FromForm2==null){
                  return;
                 }
                 if(!ctype_alnum($value1FromForm2) || !ctype_alnum($value2FromForm2) || $_SESSION['value3FromForm1']==null){
                  return;
                 }
                 global $arrayTable;
                 $i=1;
                 while(isset($arrayTable[$_SESSION['value3FromForm1']][$i]['1'])){
                  ++$i;
                 }
                  $arrayTable[$_SESSION['value3FromForm1']][$i]['1']=$value1FromForm2;
                  $arrayTable[$_SESSION['value3FromForm1']][$i]['2']=$value2FromForm2;
   
               }
      }
    }
    
    function getOrganizacijaData(){
        if($_POST==null){
          return;
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          
            if(isset($_POST['form1'])){
                $value1FromForm1 = $_POST['formInput1'];
                $value2FromForm1 = $_POST['formInput2'];
                $value3FromForm1 = $_POST['formInput3'];
                $value4FromForm1 = $_POST['formInput4'];
                if($value1FromForm1==null || $value2FromForm1==null || $value3FromForm1==null || $value4FromForm1==null){
                  return;
                }
                if(!ctype_alpha($value1FromForm1) || !ctype_alpha($value2FromForm1) || !ctype_alpha($value3FromForm1) || !ctype_digit($value4FromForm1)){
                  return;
                }
                $_SESSION['value1FromForm1']=$value1FromForm1;
                $_SESSION['value2FromForm1']=$value2FromForm1;
                $_SESSION['value3FromForm1']=$value3FromForm1;
                $_SESSION['value4FromForm1']=$value4FromForm1;
                
            }
      
              if(isset($_POST['form2'])){
                 $value1FromForm2 =$_POST['form2Input1'];
                 $value2FromForm2 =$_POST['form2Input2'];
                 if($value1FromForm2==null || $value2FromForm2==null){
                  return;
                 }
                 if(!ctype_alnum($value1FromForm2) || !ctype_alnum($value2FromForm2) || $_SESSION['value1FromForm1']==null){
                  return;
                 }
                 global $arrayTable;
                 $i=1;
                 while(isset($arrayTable[$_SESSION['value1FromForm1']][$i]['1'])){
                  ++$i;
                 }
                  $arrayTable[$_SESSION['value1FromForm1']][$i]['1']=$value1FromForm2;
                  $arrayTable[$_SESSION['value1FromForm1']][$i]['2']=$value2FromForm2;
   
               }
      }
    }
    
     function getFakturaData(){
        if($_POST==null){
          return;
        }
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
          
            if(isset($_POST['form1'])){
                $value1FromForm1 = $_POST['formInput1'];
                $value2FromForm1 = $_POST['formInput2'];
                $value3FromForm1 = $_POST['formInput3'];
                $value4FromForm1 = $_POST['formInput4'];
                if($value1FromForm1==null || $value2FromForm1==null || $value3FromForm1==null || $value4FromForm1==null){
                  return;
                }
                if(!ctype_digit($value1FromForm1) || !preg_match('/[0-9]+[\;.,][0-9]+[\.;,][0-9]+/',$value2FromForm1) || !ctype_digit($value3FromForm1) || !ctype_digit($value4FromForm1)){
                  return;
                }
                $_SESSION['value1FromForm1']=$value1FromForm1;
                $_SESSION['value2FromForm1']=$value2FromForm1;
                $_SESSION['value3FromForm1']=$value3FromForm1;
                $_SESSION['value4FromForm1']=$value4FromForm1;
                
            }
      
              if(isset($_POST['form2'])){
                 $value1FromForm2 =$_POST['form2Input1'];
                 $value2FromForm2 =$_POST['form2Input2'];
                 if($value1FromForm2==null || $value2FromForm2==null){
                  return;
                 }
                 if(!ctype_alnum($value1FromForm2) || !ctype_alnum($value2FromForm2) || $_SESSION['value1FromForm1']==null){
                  return;
                 }
                 global $arrayTable;
                 $i=1;
                 while(isset($arrayTable[$_SESSION['value1FromForm1']][$i]['1'])){
                  ++$i;
                 }
                  $arrayTable[$_SESSION['value1FromForm1']][$i]['1']=$value1FromForm2;
                  $arrayTable[$_SESSION['value1FromForm1']][$i]['2']=$value2FromForm2;
   
               }
      }
    }
?>