<?php
    session_start();
      if (isset($_SESSION['previous'])) {
        if (basename($_SERVER['PHP_SELF']) != $_SESSION['previous']) {
             session_destroy();
             session_start();
        }
    }
    $_SESSION['previous']=  basename($_SERVER['PHP_SELF']);
    include 'generator.php';
    getOrganizacijaData();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="author" content="Trajko">
		<meta charset="UTF-8">
		<title>Administracija poslovne organizacije</title>
		<link rel="stylesheet" type="text/css" href="css/mycss.css">
	</head>
	<body>
		<center>
			<div>
				<h1>Forma za unos podataka o poslovnoj organizaciji:</h1>
			</div>
		</center>
		<center>
			<div id="firstForm">
				<?php
					generateFirstForm("organizacija","Naziv organizacije","Ime vlasnika","Prezime vlasinka","Lokacija organizacije");
				?>
			</div>
		</center>	
		<center>
			<div id="secondForm">
				<h1>Spisak zaposlenih</h1>
				<?php
					generateTable("RB","NAZIV ZAPOSLENOG","NAZIV ODELJENJA","organizacija");
				?>
			</div>
		</center>

		<center>
			<div id="thirdForm">
				<h1>Unos novih radnika</h1>
				<?php
					generateSecondForm("Naziv zaposlenog","Naziv odeljenja");
				?>
			</div>
		</center>
	</body>
</html>